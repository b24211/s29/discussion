// [Section] Query Operators and Field Projection
// Looking for specific documents in our database

// [Section] Comparison Query Operators
/*
	allow us to find documents that have field number values greater than (or gte) or less than (or lte) to a specified value
	SYNTAX:
		db.collectionName.find({ field : { $gt/gte : value } });
		db.collectionName.find({ field : { $lt/lte : value } });
*/

// greater than operator
db.users.find({ age : { $gt : 50 } });
db.users.find({ age : { $gte : 50 } });

// less than operator
db.users.find({ age: { $lt: 50 } });
db.users.find({ age: { $lte: 50 } });

// $ne operator
/*
	allows us to find documents that have field number values not equal to a specified value

	SYNTAX:
		db.collectionName.find({ field : { $ne : value } })
*/
db.users.find({ age : { $ne : 82 } });

// $in operator
/*
	allows us to find documents with specific match criteria one field  using  different values

	SYNTAX:
		db.collectionName.find({ field : { $in: value } });
*/
// for single values
db.users.find({ lastName: { $in : [ "Doe" ] } });
// for multiple values
db.users.find({ firstName: { $in : [ "Jane", "Stephen" ] } });

// [Section] Logical Query Operators
// $or operator
/*
	allows us to find documents that match a single criteria from multiple provided search criteria 

	SYNTAX:
		db.collectionName.find({ $or: [ { FieldA : valueA }, { fieldB : valueB } ] });
*/
db.users.find({ $or: [ { firstName : "Neil" }, { age : 25 } ] });

// $and operator
/*
	allows us to find documents matching multiple criteria in a single field

	SYNTAX:
		db.collectionName.find({ $and: [ { FieldA : valueA }, { fieldB : valueB } ] });
*/
db.users.find({ $and : [ { age : { $ne : 82 } }, { age : { $ne: 76 } } ] });

// [Section] Field Projection
/*
	- retrieving documents are common operations we do and by default MongoDB queries return the whole document as a response
	- there might be instances when fioelds are not useful for the query that we are doing
	- we can try to include/exclude fields from the response
*/
// inclusion
/*
	- allows us to add specific fields only when retrieving documents
	- the value 1 is to denote that the field is being included
	- SYNTAX:
		db.users.find({criteria},{field:1});
*/
db.users.find(
	{firstName: "Jane"},
	{
		firstName: 1,
		lastName: 1,
		contact: 1
	}
);

// exclusion
/*
	- allows us to remove specific fields when retrieving documents.
	- the value 0 is to denote that the field is being excluded
	- SYNTAX
		db.users.find({criteria},{field:0});
*/
db.users.find(
	{firstName: "Jane"},
	{
		contact: 0,
		department: 0
	}
);

// suppressing the ID field
/*
	- in general, when using field projection, field inclusion and exclusion may not be used at the same time.
	- allows us to exclude "_id" field when retrieving documents
	SYNTAX:
		db.users.find({criteria},{_id: 0})
*/
db.users.find(
	{firstName: "Jane"},
	{
		_id:0,
		firstName: 1,
		lastName: 1,
	}
);

// accessing fields inside embedded documents
// inclusion
db.users.find(
	{firstName: "Jane"},
	{
		firstName: 1,
		"contact.phone": 1
	}
);
// exclusion
db.users.find(
	{firstName: "Jane"},
	{
		firstName: 0,
		"contact.phone": 0
	}
);

/*
documents to be added to be used for $slice
db.users.insert({
    namearr: [
        {
            namea: "juan"
        },
        {
            nameb: "tamad"
        }
    ]
});	
*/

// field projection for specific array elements
// $slice operator - allows us to retrieve an element that matches the search criteria
db.users.find(
	{"namearr":
		{namea: "juan"}
	},
	{
		returnedArr:
			{$slice: 1}
	}
);

// [Section] Evaluation Querry Operator
// $regex operator - allows us to find documents that match a specific string pattern using regular expressions
// SYNTAX:
// 	db.collectionName.find({ field : { $regex : 'pattern', "$optionValue" } })

// case sensitive
db.users.find({ firstName:{ $regex: "N" } });
// case insensitive
db.users.find({ firstName:{ $regex: "j", $options : "$i" } });
